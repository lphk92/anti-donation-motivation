import os
import json
import pandas as pd

from flask import render_template, redirect, request
from server.model import Workout, WorkoutLog, WorkoutState

from server import app

# Load default events (parse from google calendar)
with open("server/events.json", "r") as f:
    default_workouts = [Workout.from_json(j) for j in json.loads(f.read())]

# Data configuration
data_dir = "server/data/"
melany_file = os.path.join(data_dir, "melany.csv")
lucas_file = os.path.join(data_dir, "lucas.csv")

# Load melany data
if os.path.isfile(melany_file):
    melany_log = WorkoutLog.read(melany_file)
else:
    melany_log = WorkoutLog("melany", default_workouts)
    melany_log.save(data_dir)

# Load lucas data
if os.path.isfile(lucas_file):
    lucas_log = WorkoutLog.read(lucas_file)
else:
    lucas_log = WorkoutLog("lucas", default_workouts)
    lucas_log.save(data_dir)


@app.route("/")
def index():
    return render_template("index.html",
                           melany_log=melany_log,
                           lucas_log=lucas_log)

@app.route("/update_workout", methods=["POST"])
def update_workout():
    user = request.form.get('user')
    date = request.form.get('date')
    description = request.form.get('description')
    state = request.form.get('state')

    if user == "melany":
        workout = melany_log.get_workout(date, description)
        workout.state = WorkoutState[state]
        melany_log.save(data_dir)
    elif user == "lucas":
        workout = lucas_log.get_workout(date, description)
        workout.state = WorkoutState[state]
        lucas_log.save(data_dir)
    else:
        print("ERROR: Invalid user: {}".format(user))

    return redirect("/")


if __name__ == "__main__":
    app.run()
