import datetime
import json
import os
import pandas as pd
from enum import Enum

DT_FORMAT = "%Y-%m-%d"

def today():
    return datetime.datetime.now().strftime(DT_FORMAT)

class WorkoutState(Enum):
    FAILED = 1
    COMPLETED = 2
    LEGIT_EXCUSE = 3
    REST_MAKEUP = 4

class Workout:
    def __init__(self, date, description, state=WorkoutState.FAILED):
        self.date = date
        self.description = description
        self.state = state

    def __repr__(self):
        return "<Workout: {} --- {} --- {}>".format(self.date,
                                                    self.state.name,
                                                    self.description)

    @staticmethod
    def from_json(json_data):
        if 'state' in json_data:
            state = WorkoutState[json_data['state']]
        else:
            state = WorkoutState.FAILED

        return Workout(date=json_data['date'],
                       description=json_data['description'],
                       state = state)

    def to_json(self):
        return json.dumps({
            'date': self.date,
            'description': self.description,
            'state': self.state.name,
        })

class WorkoutLog:
    def __init__(self, name, workouts):
        self.name = name
        self.workouts = workouts

    def get_daily_workouts(self, date=None):
        if date is None:
            date = today()

        return [w for w in self.workouts if w.date == date]

    def get_past_workouts(self):
        return sorted([w for w in self.workouts if w.date < today()],
                      key = lambda w: w.date,
                      reverse = True)

    def get_failed_workouts(self):
        return [w for w in self.workouts if w.state == WorkoutState.FAILED and
                                            w.date < today()]

    def get_workout(self, date, description):
        for w in self.workouts:
            if w.date == date and w.description == description:
                return w

    @property
    def balance(self):
        bad_workouts = self.get_failed_workouts()
        retval = sum([i for i in range(len(bad_workouts) + 1)])
        return retval

    def to_df(self):
        return pd.DataFrame([json.loads(w.to_json()) for w in self.workouts])\
                 .sort_values(by='date')

    def save(self, save_dir):
        self.to_df().to_csv(os.path.join(save_dir, "{}.csv".format(self.name)))

    @staticmethod
    def read(filename):
        df = pd.read_csv(filename)
        return WorkoutLog(name=os.path.splitext(os.path.basename(filename))[0],
                          workouts = [Workout.from_json(j)
                                      for j in json.loads(df.to_json(orient='records'))])
