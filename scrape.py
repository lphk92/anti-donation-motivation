# coding: utf-8
import datetime
import pandas as pd
import json

with open("plan.ics", "r") as f:
    lines = f.readlines()

events = list()

new_event = None
in_event = False

for line in lines:
    if line.startswith("BEGIN:VEVENT"):
        in_event = True
        new_event = dict()

    elif line.startswith("END:VEVENT"):
        in_event = False
        if new_event is not None and 'date' in new_event:
            events.append(new_event)
            new_event = None

    if in_event:
        val = line.split(":")[-1].strip()

        if val.startswith("Training"):
            in_event = False
            new_event = None
            continue

        try:
            if line.startswith("DTSTART"):
                new_event['date'] = datetime.datetime.strptime(val, '%Y%m%d')
            elif line.startswith("SUMMARY"):
                new_event['description'] = val
            elif line.startswith("RRULE"):
                new_event['repeat'] = val
        except:
            continue

# Process reptition into individual events
for event in events:
    if 'repeat' in event:
        params = {p.split('=')[0]: p.split('=')[1]
                  for p in event['repeat'].split(';')}

        for i in range(int(params['COUNT']) - 1):
            new_date = event['date'] + datetime.timedelta(days=7 * (i+1))
            events.append({
                'description': event['description'],
                'date': new_date
            })

        del event['repeat']

for event in events:
    event['date'] = event['date'].strftime("%Y-%m-%d")

with open('events.json', 'w') as f:
    f.write(json.dumps(events))

df = pd.DataFrame(events).sort_values(by='date')
